# README #


### What is this repository for? ###

* Create a RiskArq website, which looks same as www.acl.com

### How do I get set up? ###

* Please clone the repository to your visual studio
* Please create a separate html page for each user under your name
* Please work on Bootstrap 4 and include the CDN links. Do not edit CDN files

### Contribution guidelines ###

* Raj - Navbar, fixed icons, carousal(blue pane), scroll spy, BG world map
* Alex - Sec1(white) - enchance work, orange bg transperant, mgt icons
* nitish - media(video) section, footer, blog
* Anudeep - Scrollable logos(hzl) + Green bg, reasons to love s/w
* sushanth - whats going on, get in touch

### Who do I talk to? ###

* Please contact Aravind/Sharad
